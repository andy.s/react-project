// react packages
import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

// router requirements
import history from './history'
// import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import {HashRouter, Switch, Route, Redirect} from 'react-router-dom'


// material ui components / packages
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import mauiTheme from './maui-theme'

// styling
import './index.css';
// component
import App from './components/App'

// store and redux implementation
import { createStore } from 'redux'
import { Provider } from 'react-redux'  // provides store access for all components
import rootReducer from './reducers/index'

const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

ReactDOM.render(
    <MuiThemeProvider theme={mauiTheme}>
        <Provider store={store}>
            <App />
        </Provider>
    </MuiThemeProvider>,
    document.getElementById('root')
);

registerServiceWorker();
