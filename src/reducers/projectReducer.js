// get json source file
import Projects from '../data/projects'
// set file as initial State (initial State has no separate action)
const initialStateProjects = Projects

// a reducer is always a pure function (note red1)
function projectReducer( state = initialStateProjects, action ){

    switch (action.type){




        default:
            return state
    }

}

export default projectReducer

// red1
// Things you should never do inside a pure function (reducer):
// - Mutate its arguments;
// - Perform side effects like API calls and routing transitions;
// - Call non-pure functions, e.g. Date.now() or Math.random().
// reducer principe => KISS (Keep It Simple, Stupid)
// https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-pure-function-d1c076bec976